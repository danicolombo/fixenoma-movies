/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/


const apikey = require('./api-key');
var APIKEY = apikey.APIKEY;

var axios = require('axios')
var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


/**********************
 * Example get method *
 **********************/

app.get('/movieapi/:page', function (req, res) {

  axios.get(`http://www.omdbapi.com/?apikey=${APIKEY}&s=title&page=${req.params.page}`)
    .then((response) => {
      res.json({
        movies: response.data,
        headers: response.headers,
        status: response.status,
        statusText: response.statusText,
        config: response.config,
        page: req.params.page,
      });
    })
    .catch((err) => {
      res.json({
        error: err,
        movies: null
      });
    })

});

app.get('/movieapi/movie/:id', function (req, res) {
  // Add your code here
  var url = `http://www.omdbapi.com/?apikey=${APIKEY}&i=${req.params.id}&plot=full`

  axios.get(url)
    .then((response) => {
      res.json({
        movie: response.data,
        headers: response.headers,
        status: response.status,
        statusText: response.statusText,
        config: response.config
      });
    })
    .catch((err) => {
      res.json({
        error: err,
        movie: null
      });
    })

});



/****************************
* Example post method *
****************************/

app.post('/movieapi', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/movieapi/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example put method *
****************************/

app.put('/movieapi', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/movieapi/*', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

/****************************
* Example delete method *
****************************/

app.delete('/movieapi', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/movieapi/*', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
