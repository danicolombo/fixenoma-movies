import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

const finexonaBlack = "#0C0B10";
const finexonaPink = "#FC676B";

let theme = createMuiTheme({
    palette: {
        primary: {
            main: `${finexonaBlack}`,
        },
        secondary: {
            main: `${finexonaPink}`,
        },
    },
    typography: {
        h1: {
            fontWeight: 900,
            fontFamly: 'Raleway',
            fontSize: '4rem',
            paddingTop: '5px',
        },
        h2: {
            fontWeight: 300,
            fontFamly: 'Raleway',
            fontSize: '4rem',
            paddingTop: '5px',
            color: 'red',
        },
        h3: {
            fontWeight: 300,
            fontFamly: 'Raleway',
            fontSize: '2rem',
            paddingTop: '5px',
        },
        h4: {
            fontWeight: 500,
            fontFamly: 'Raleway',
            fontSize: '24px',
            paddingTop: '5px',
        },
        h5: {
            fontWeight: 400,
            fontFamly: 'Raleway',
            fontSize: '14px',
            paddingTop: '5px',
        },
        h6: {
            fontWeight: 400,
            fontFamly: 'Raleway',
            fontSize: '12px',
            paddingTop: '5px',
        },
        body1: {
            fontSize: '16px',
            fontWeight: 600,
            fontFamly: 'Raleway',
            paddingTop: '5px',
            color: '#FFF',
        },
        body2: {
            fontSize: '14px',
            fontWeight: 300,
            fontFamly: 'Raleway',
            paddingTop: '5px',
            color: '#FFF',
        },
    },
});

theme = responsiveFontSizes(theme);

export default theme;
