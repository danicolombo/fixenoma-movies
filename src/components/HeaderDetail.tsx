import React from 'react';
import {Grid, Box, Typography, Button} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {useHistory} from 'react-router-dom';
import PosterTitlesMinimal from './PosterTitlesMinimal';
import alien from '../assets/alien-film.jpg';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        zIndex: 1,
    },
    bg: {
        position: 'absolute',
        zIndex: -1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
        backgroundImage: `url(${alien})`,
        backgroundSize: 'cover',
        height: '350px',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '100% 55%',
        opacity: '0.4',
        [theme.breakpoints.only('sm')]: {
            height: '400px',
        },
        [theme.breakpoints.only('xs')]: {
            height: '550px',
        }
    },
    box: {
        padding: theme.spacing(6),
    },
    buttons: {
        textAlign: 'right',
        [theme.breakpoints.only('xs')]: {
            textAlign: 'left',
            padding: '0px',
            '& *': {
                padding: '0px',
            }
        },
    },
    highlight: {
        color: 'red',
        cursor: 'pointer',
        [theme.breakpoints.only('sm')]: {
            padding: theme.spacing(1),
        },
    }
}));

const HeaderDetail: React.FC = () => {
    const classes = useStyles();
    let history = useHistory();

    const handleClick = () => {
        history.push('/')
    };

    return (
        <Box className={classes.root}>
            <Box className={classes.bg}/>
            <Grid container className={classes.box}>
                <Grid item sm={6} xs={12} onClick={handleClick}>
                    <Typography variant="h4" component="p">Fixenova <span className={classes.highlight}>Movies</span></Typography>
                </Grid>
                <Grid item sm={6} xs={12} className={classes.buttons}>
                    <Button color="inherit" href="https://www.fixenova.com/">Fixenova</Button>
                    <Button color="inherit" href="https://procnedc.github.io/resume/">Dev</Button>
                    <Button color="inherit" href="https://gitlab.com/danicolombo">Gitlab</Button>
                </Grid>
            </Grid>
            <PosterTitlesMinimal />
        </Box>
    );
}

export default HeaderDetail;
