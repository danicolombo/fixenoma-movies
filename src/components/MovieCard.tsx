import React, {useState, useEffect} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import {Card, Grid, CardHeader, CardMedia, IconButton} from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import noPoster from '../assets/no-film.jpg';
import {useHistory} from 'react-router-dom';
import {useStickyState} from '../hooks/UseStickyState';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: 'inherit',
            padding: theme.spacing(3),
            margin: theme.spacing(1),
            cursor: 'pointer',
            display: 'flex',
            flexDirection: 'column',
            height: '450px',
        },
        media: {
            height: 0,
            padding: '56% 0',
        },
        header: {
            '& *': {
                fontSize: '16px',
                color: '#FFF',
            },
            '& * + *': {
                fontSize: '14px',
            }
        },
        heart: {
            fontSize: '2rem',
            color: '#FFF',
        },
        fabButton: {
            display: 'flex',
            justifyContent: 'center',
        },
        card: {
            flexGrow: 1,
        }
    }),
);

const MovieCard: React.FC<{
    movie: any;
    key: string;
}> = ({ movie }) => {
    const classes = useStyles();
    const [like, setLike] = useState(false);
    let history = useHistory();

    const [myMoviesSession, setMyMoviesSession] = useStickyState([], "myMovies");

    useEffect(() => {
        if (myMoviesSession.find((sessionMovie: string) => sessionMovie === movie.imdbID)){
            setLike(true);
        }
    }, [])

    const handleClick = (imdbID: string) => {
        if (myMoviesSession.find((movie: string) => movie == imdbID)){
            setMyMoviesSession(myMoviesSession.filter((movie: any) => movie !== imdbID));
            setLike(false);
        }else{
            setMyMoviesSession([...myMoviesSession, imdbID]);
            setLike(true);
        }   
    };

    const handleDetail = (id: string) => {
        history.push(`/detalle/?id=${id}`)
    };

    return (
        <Grid item md={3} sm={6} xs={12} justify={'center'}>
            <Card className={classes.root}>
                <Grid container className={classes.card}>
                    <Grid item md={9}>
                        <CardHeader className={classes.header}
                            title={movie.Title}
                            subheader={movie.Year}
                        />
                    </Grid>
                        <Grid item md={3} className={classes.fabButton}>
                            <IconButton aria-label="add to favorites" onClick={() => handleClick(movie.imdbID)}>
                                {like ? <FavoriteIcon className={classes.heart} /> : <FavoriteBorderIcon className={classes.heart}/>}
                            </IconButton>
                        </Grid>
                    </Grid>
                    <CardMedia
                        onClick={() => handleDetail(movie.imdbID)}
                        className={classes.media}
                        image={(movie.Poster == 'N/A') ? noPoster : movie.Poster}
                        title={movie.Title}
                    />
            </Card>
        </Grid>
    );
}

export default MovieCard;
