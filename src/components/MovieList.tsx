import {API} from 'aws-amplify';
import React, {useState, useEffect} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MovieCard from './MovieCard';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            padding: theme.spacing(3),
        },
        pager: {
            display: 'flex',
            justifyContent: 'center',
        }
    }),
);

const MovieList: React.FC = () => {
    const classes = useStyles();
    const [movies, setMovies] = useState<string[]>([]);

    const [page, setPage] = useState(1);
    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };

    useEffect(() => {
        API.get('movieAPI', `/movieapi/${page}`, { page })
            .then((res) => {
                setMovies(res.movies.Search)
            })
            .catch((err) => console.log(err))
    }, [page])

    return (
        <Grid container className={classes.container}>
            {movies?.map((movie: any) => (
                <MovieCard movie={movie} key={movie.imdbID}/>
            ))}
            <Grid item sm={12}>
                <Pagination count={10} page={page} onChange={handleChange} color="primary" className={classes.pager}/>
            </Grid>
        </Grid>
    );
}

export default MovieList;
