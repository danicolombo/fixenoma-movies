import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {ThemeProvider} from '@material-ui/core/styles';
import theme from './theme';
import Main from './components/Main';
import MovieDetail from './components/MovieDetail';

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={() => <Main />}/>
          <Route exact path='/detalle' component={() => <MovieDetail />}/>
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
